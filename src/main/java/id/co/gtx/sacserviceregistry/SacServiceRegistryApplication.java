package id.co.gtx.sacserviceregistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class SacServiceRegistryApplication {

	public static void main(String[] args) {
		SpringApplication.run(SacServiceRegistryApplication.class, args);
	}

}
